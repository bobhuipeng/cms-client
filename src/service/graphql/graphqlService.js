import axios from 'axios'


const axiosGitHubGraphQL = axios.create({
  baseURL: 'http://localhost:4000/graphql',
});

const GET_HOMEPAGE = `
  {
    homepage{
      heading
      subheading
      heroImageUrl
    }
  }
`

const GET_FAQS = `
  {
    faqs{
      title
      body
    }
  }
  `

 const getAllFaqs = async () => {

  let result 
  try {
    let resp =  await axiosGitHubGraphQL.post('', { query: GET_FAQS })
    if(resp.data && resp.data.data){
      result = resp.data.data.faqs
    }
  } catch (error) {
    console.log(error)
  }
  return result
}

const getHomepage = async () => {
  let result 
  try {
    let resp =  await axiosGitHubGraphQL.post('', { query: GET_HOMEPAGE })
    if(resp.data && resp.data.data){
      result = resp.data.data.homepage
    }
  } catch (error) {
    console.log(error)
  }
  return result
}

export {
  getAllFaqs,
  getHomepage,
}
   