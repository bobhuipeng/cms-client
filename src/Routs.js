import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

// TOP LEVEL CONTAINERS
import Main from './components/Main'
import Faq from './components/faqs/Faq'


export class Routes extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
  }


  render() {
    return (
      <section className="route-container">

        <Switch>
          <Route path='/' exact render={props => ( <Main props={props} a={123}/> )} />
          <Route path='/frqs' exact render={props => ( <Faq props={props}/> )} />
        </Switch>

      </section>
    );
  }
}



export default Routes;
