import React, { useContext, useReducer } from 'react';
import logo from './logo.svg';
import './App.css';
import Routs from './Routs'

import reducer from './reducer'
import CmsContext from './context'

import Main from './components/Main'

// const initState = {
//   homepage:{
//     data:{

//     },
//     isLoading: false
//   },
//   faqs:{
//     data: [],
//     isLoading: false
//   }
// }

// const reducer = (state,action) => {
//   switch(action.type){
//     case "UDPATE_HOMEPAGE": return {...state, homepage:{...state.homepage,...action.payload}}
//     case "UDPATE_FAQS": return {...state, faqs:{...state.faqs,...action.payload}}
//     default: return {...state}
//   }
// }



const App = (params) =>{
  const initState = useContext(CmsContext)
  const [state,dispatch] = useReducer(reducer,initState)
    return (
      <CmsContext.Provider value={{state,dispatch}}>
        <div className="App">
          <Routs/>
        </div>
      </CmsContext.Provider>
    );
}

export default App;
