const reducer = (state,action) => {
  switch(action.type){
    case "UDPATE_HOMEPAGE": return {...state, homepage:{...state.homepage,...action.payload}}
    case "UDPATE_FAQS": return {...state, faqs:{...action.payload}}
    case "UDPATE_CURRENT_FAQ": return {...state, currentFaq:{...action.payload}}
    default: return {...state}
  }
}

export default reducer