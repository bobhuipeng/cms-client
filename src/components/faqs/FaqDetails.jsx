import React, { useContext } from 'react'

import CmsContext from '../../context'

const FaqDetails = (params) => {
  const { state } = useContext(CmsContext)
  return (
    <div>
      <h3>
        {state.currentFaq.title}
      </h3>
      <div>
        
        {/* TODO: the source code include html tag is not safe.  */}
        <div dangerouslySetInnerHTML={{__html: state.currentFaq.body}} />
      </div>
    </div>
  )
}

export default FaqDetails
