import React, {  useContext } from 'react'

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AssignmentIcon from '@material-ui/icons/Assignment';

import CmsContext from '../../context'

const FaqList = (params) => {
  const { state, dispatch } = useContext(CmsContext)

  const updateFaqDetail = (faq) => {
    dispatch({ type: 'UDPATE_CURRENT_FAQ', payload: faq })
  }

  return (
    <div>
      <List>
        {state.faqs && state.faqs.data && state.faqs.data.map((_faq, idx) =>
          <ListItem key={idx} button onClick={() => updateFaqDetail(_faq)}>
            <ListItemIcon>
              <AssignmentIcon />
            </ListItemIcon>
            <ListItemText primary={_faq.title} />
          </ListItem>
        )}
      </List>
    </div>
  )
}


export default FaqList