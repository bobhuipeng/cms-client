import React, { useEffect, useState, useContext } from 'react'
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import { getAllFaqs } from '../../service/graphql/graphqlService'
import SideNav from '../nav/SideNav'

import FaqDetails from './FaqDetails'
import FaqList from './FaqList'
import CmsContext from '../../context'

const styles = theme => ({
  faqContainer: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: '63px',
    flexGrow: 1,

  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    height: '350px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  textcontent: {
    flexGrow: 1,
  },
});

const Faq = (params) => {
  const { classes } = params;

  const { state, dispatch } = useContext(CmsContext)

  useEffect(() => {
    getAllFaqsAsync()
  }, [])

  const getAllFaqsAsync = async () => {
    dispatch({ type: 'UDPATE_FAQS', payload: { data: state.faqs.data, isLoading: true } })
    let results = await getAllFaqs()
    dispatch({ type: 'UDPATE_FAQS', payload: { data: results, isLoading: false } })
    if (results && results.length > 0) {
      dispatch({ type: 'UDPATE_CURRENT_FAQ', payload: results[0] })
    }
  }

  return (
    <SideNav history={params.props.history}>
      <div className={classes.faqContainer}>
        <Grid container spacing={8}>
          <Grid item xs={12} sm={6}>
            <Paper className={classes.paper}><FaqDetails></FaqDetails></Paper>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Paper className={classes.paper}><FaqList></FaqList></Paper>
          </Grid>
        </Grid>
      </div>
    </SideNav>
  )
}

export default withStyles(styles)(Faq)
