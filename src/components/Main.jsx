import React, { useEffect, useState } from 'react'
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import SideNav from './nav/SideNav'

import { getHomepage } from '../service/graphql/graphqlService'

import hero from '../assets/hero.jpg'

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: '43px'
  },
  textContent: {
    marginTop: '-550px'
  },
  bgImg:{
    width: '100%',
  }
});

const Main = (params) => {
  const { classes } = params;

  const [homepage, setHomepage] = useState([])
  useEffect(() => {
    getHomepageAsync()

  }, [])

  const getHomepageAsync = async () => {
    let results = await getHomepage()
    console.log(results)
    setHomepage(results)
  }

  const updatRoute = (route) => {
    params.props.history.push('/' + route);
  }

  return (

    <SideNav history={params.props.history}>
      <div className={classes.root}>
        <img src={homepage.heroImageUrl} className={classes.bgImg}></img>
        <div className={classes.textContent}>
          <h1>{homepage.heading}</h1>
          <h2>{homepage.subheading}</h2>
        </div>
        <Button onClick={() => updatRoute('frqs')}>Learn more</Button>
      </div>
    </SideNav>


  )
}

export default withStyles(styles)(Main)