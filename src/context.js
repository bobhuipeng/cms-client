import React, { PureComponent } from 'react'


const CmsContext = React.createContext({
  homepage:{
    data:{

    },
    isLoading: false
  },
  faqs:{
    data: [],
    isLoading: false
  },
  currentFaq:{
      title: "",
      body: ""
  }
})

export default CmsContext